import {gql} from "apollo-boost";

const GET_USER = gql`
  query getUser($id: String!) {
    user(id: $id) {
      id,
      first_name,
      last_name,
      email,
      avatar,
    }
  }
`;

export {
  GET_USER
}
