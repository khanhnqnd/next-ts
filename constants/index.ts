const apiBaseURL = "http://localhost:5000";
const googleLogin = `${apiBaseURL}/auth/google`;
const apiGraphQLURL = `${apiBaseURL}/graphql`;

export {
  apiBaseURL,
  googleLogin,
  apiGraphQLURL
}
