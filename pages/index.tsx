import React, {useEffect} from 'react';

import Layout from "../components/layout";
import {NextPage} from "next";
import fetch from "isomorphic-unfetch";
import axios from "axios";
import {ApolloProvider} from "@apollo/react-hooks";

import {apolloClient} from "../utils";

interface HomeProps {
  home: string;
}

const Home: NextPage<HomeProps> = (props: HomeProps) => {
  return (
    <div>
      <ApolloProvider client={apolloClient}>
        <Layout>
          <div>Hello {props.home}</div>
        </Layout>
      </ApolloProvider>
    </div>
  )
};
Home.getInitialProps = async () => {
  return {
    home: "default"
  }
};

export default Home;
