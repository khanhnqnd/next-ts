import React, {useState, useEffect} from "react";

import Layout from "../components/layout";
import {NextPage} from "next";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { makeStyles, Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

interface ProfileProps {

}

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function a11yProps(index: any) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}
function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
}));

const basicInfoDefault = {
  id: "",
  email: "",
  avatar: "",
  first_name: "",
  last_name: "",
  intro: "",
  birthday: "",
  gender: "",
};

const Profile: NextPage = () => {

  const classes = useStyles();
  const [basicInfo, setBasicInfo] = useState(basicInfoDefault);

  useEffect(() => {
    let user = localStorage.getItem('user');
    if (user) {
      let userParse = JSON.parse(user);
      setBasicInfo(userParse);
    }
  }, []);

  const [value, setValue] = React.useState(0);
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const [gender, setGender] = React.useState('');
  const handleChangeGender = (event: React.ChangeEvent<{ value: unknown }>) => {
    setGender(event.target.value as string);
  };


  const handleChangeBasicInfo = () => {

  };

  const handleChangeContactInfo = () => {

  };

  const handleChangePassword = () => {

  };

  const handleChangeNotification = () => {

  };

  return (
    <div>
      <Layout>
        <div className={classes.root}>
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={value}
            onChange={handleChange}
            aria-label="Vertical tabs example"
            className={classes.tabs}
          >
            <Tab label="Cài đặt cơ bản" {...a11yProps(0)} />
            <Tab label="Cài đặt liên hệ" {...a11yProps(1)} />
            <Tab label="Mật khẩu và bảo mật" {...a11yProps(2)} />
            <Tab label="Cài đặt thông báo" {...a11yProps(3)} />
          </Tabs>
          <TabPanel value={value} index={0}>
            <TextField
              id="standard-full-width"
              label="Họ và đệm"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              value={basicInfo.last_name}
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="standard-full-width"
              label="Tên"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              value={basicInfo.first_name}
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="standard-full-width"
              label="Email"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              value={basicInfo.email}
              disabled
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="outlined-textarea"
              label="Giới thiệu"
              placeholder=""
              style={{ margin: 8 }}
              multiline
              fullWidth
              value={basicInfo.intro}
              className={classes.textField}
              margin="normal"
              variant="outlined"
            />
            <TextField
              id="date"
              label="Ngày sinh"
              type="date"
              style={{ margin: 8 }}
              fullWidth
              margin="normal"
              value={basicInfo.birthday}
              defaultValue="2017-05-24"
              className={classes.textField}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <Select value={basicInfo.gender} onChange={handleChangeGender} displayEmpty>
              <MenuItem value="">
                <em>Giới tính</em>
              </MenuItem>
              <MenuItem value={"Male"}>Nam</MenuItem>
              <MenuItem value={"Female"}>Nữ</MenuItem>
              <MenuItem value={"Other"}>Khác</MenuItem>
            </Select>
            <br/>
            <Button variant="contained" onClick={() => handleChangeBasicInfo()} color="primary">
              Lưu
            </Button>
          </TabPanel>
          <TabPanel value={value} index={1}>
            <TextField
              id="standard-full-width"
              label="Điện thoại"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="standard-full-width"
              label="Địa chỉ"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="standard-full-width"
              label="Website"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="standard-full-width"
              label="Facebook"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="standard-full-width"
              label="Zalo"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="standard-full-width"
              label="Skype"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="standard-full-width"
              label="Viber"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <Button variant="contained" color="primary">
              Lưu
            </Button>
          </TabPanel>
          <TabPanel value={value} index={2}>
            <TextField
              id="standard-full-width"
              label="Mật khẩu cũ"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="standard-full-width"
              label="Mật khẩu mới"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="standard-full-width"
              label="Nhập lại mật khẩu"
              style={{ margin: 8 }}
              placeholder=""
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <Button variant="contained" color="primary">
              Lưu
            </Button>
          </TabPanel>
          <TabPanel value={value} index={3}>
            <Select value={gender} onChange={handleChangeGender} displayEmpty>
              <MenuItem value="">
                <em>Phát âm thanh khi có thông báo</em>
              </MenuItem>
              <MenuItem value={"Male"}>Bật</MenuItem>
              <MenuItem value={"Female"}>Tắt</MenuItem>
            </Select>
            <br/>
            <Select value={gender} onChange={handleChangeGender} displayEmpty>
              <MenuItem value="">
                <em>Nhận thông báo</em>
              </MenuItem>
              <MenuItem value={"Male"}>Bật</MenuItem>
              <MenuItem value={"Female"}>Tắt</MenuItem>
            </Select>
            <br/>
            <Button variant="contained" color="primary">
              Lưu
            </Button>
          </TabPanel>
        </div>
      </Layout>
    </div>
  )
};
export default Profile;
