import React from "react";

import Header from "../header";
import Footer from "../footer";

interface LayoutProps {
  children: React.ReactChild
}

const Layout = (props: LayoutProps) => {
  return (
    <div>
      <Header/>
      {props.children}
      <Footer/>
    </div>
  )
};

export default Layout;
