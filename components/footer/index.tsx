import React, {FunctionComponent} from 'react';
import {makeStyles, Theme} from "@material-ui/core/styles";
import {Container, Typography, Link} from '@material-ui/core';
const useStyles = makeStyles((theme: Theme) => ({
  footer: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing(8),
    padding: theme.spacing(6, 0),
  },
}));

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://nhadatmoi.net/">
        Nhadatmoi
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const Footer: FunctionComponent = () => {
  const classes = useStyles();

  return (
    <div>
      <footer className={classes.footer}>
        <Container maxWidth="lg">
          <Typography variant="h6" align="center" gutterBottom>
            Nhà đất mới
          </Typography>
          <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
            348 Tố Hữu
          </Typography>
          <Copyright />
        </Container>
      </footer>
    </div>
  )
};

export default Footer;
