import React, {FunctionComponent, useEffect, useState} from 'react';
import {makeStyles, Theme} from "@material-ui/core/styles";
import {
  Container,
  Button,
  Typography,
  Toolbar,
  Avatar,
  Popover,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Divider,
  ListItemIcon,
} from "@material-ui/core";

import LoginDialog from "../login";
import {useQuery} from "@apollo/react-hooks";
import {GET_PROFILE, LOG_OUT} from "../../graphql/profile/queries"
import {apolloClient} from "../../utils";
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SettingsIcon from '@material-ui/icons/Settings';
import ListIcon from '@material-ui/icons/List';


const useStyles = makeStyles((theme: Theme) => ({
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarItem: {
    marginRight: theme.spacing(1)
  },
  typography: {
    padding: theme.spacing(2),
  },
  inline: {
    display: "inline"
  }
}));

const defaultUser = {
  id: "",
  email: "",
  avatar: "",
  first_name: "",
  last_name: ""
};

const Header: FunctionComponent = () => {

  const classes = useStyles();
  const [openLogin, setOpenLogin] = useState(false);

  const [userInfo, setUserInfo] = useState(defaultUser);

  // const {data, loading, error} = useQuery(GET_USER, {variables: {id: "5ddd016ea469b772a022021a"}});

  useEffect(() => {
    let user = localStorage.getItem('user');
    if (user) {
      setUserInfo(JSON.parse(user));
    } else {
      apolloClient.query({query: GET_PROFILE})
        .then(r => {
          let user = r.data.profile;
          console.log(r);
          if (user) {
            localStorage.setItem('user', JSON.stringify(user));
            setUserInfo(user);
          }
        }).catch(e => ({}))
    }
  }, []);

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const logout = () => {
    localStorage.removeItem('user');
    setUserInfo(defaultUser);
    apolloClient.query({query: LOG_OUT})
      .then(r => console.log(r));
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div>
      <Container maxWidth={"lg"}>
        <Toolbar className={classes.toolbar}>
          <Button size="small" href={"/"}>Nhà đất mới</Button>
          <Typography
            component="h6"
            variant="h5"
            color="inherit"
            align="center"
            noWrap
            className={classes.toolbarTitle}
          >

          </Typography>
          <Button href="/profile" className={classes.toolbarItem}>
            Dự án
          </Button>
          <Button href="#text-buttons" className={classes.toolbarItem}>
            Tin rao
          </Button>
          {
            userInfo.id ? (
              <div>
                <Button aria-describedby={id} onClick={handleClick}>
                  <Avatar alt={userInfo.first_name} src={userInfo.avatar}/>
                </Button>
                <Popover
                  id={id}
                  open={open}
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                  }}
                >
                  <Typography className={classes.typography}>
                    <ListItem alignItems="flex-start">
                      <ListItemAvatar>
                        <Avatar alt={userInfo.first_name} src={userInfo.avatar} />
                      </ListItemAvatar>
                      <ListItemText
                        primary={`${userInfo.first_name} ${userInfo.last_name}`}
                        secondary={
                          <React.Fragment>
                            <Typography
                              component="span"
                              variant="body2"
                              className={classes.inline}
                              color="textPrimary"
                            >
                              {userInfo.email}
                            </Typography>
                          </React.Fragment>
                        }
                      />
                    </ListItem>
                    <Divider/>
                    <ListItem button>
                      <ListItemIcon>
                        <GroupAddIcon />
                      </ListItemIcon>
                      <ListItemText primary="Nâng cấp" />
                    </ListItem>
                    <ListItem button>
                      <ListItemIcon>
                        <ListIcon />
                      </ListItemIcon>
                      <ListItemText primary="Quản lý tin rao" />
                    </ListItem>
                    <Divider/>
                    <ListItem button>
                      <ListItemIcon>
                        <SettingsIcon />
                      </ListItemIcon>
                      <ListItemText primary="Cài đặt"/>
                    </ListItem>
                    <ListItem button onClick={() => logout()}>
                      <ListItemIcon>
                        <ExitToAppIcon />
                      </ListItemIcon>
                      <ListItemText primary="Đăng xuất" />
                    </ListItem>
                  </Typography>
                </Popover>
              </div>
            ) : (
              <Button onClick={() => setOpenLogin(true)} className={classes.toolbarItem}>
                Đăng nhập
              </Button>
            )
          }
          <Button variant="outlined" color="primary" size="small">
            Đăng tin
          </Button>

        </Toolbar>
      </Container>
      <LoginDialog onClose={() => setOpenLogin(false)} open={openLogin}/>
    </div>
  )
};

export default Header;
